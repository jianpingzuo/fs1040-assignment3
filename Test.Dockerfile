FROM node:lts

WORKDIR /api

COPY  package*.json ./

RUN npm install

COPY . ./


COPY wait-for-it.sh ./wait-for-it.sh 
RUN chmod +x wait-for-it.sh


CMD ./wait-for-it.sh db:3306 --strict --timeout=300 -- npm run test