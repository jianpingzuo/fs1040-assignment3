FROM node:lts

WORKDIR /app

COPY  package*.json ./

RUN npm install

COPY . ./

EXPOSE 8080
#COPY wait-for-it.sh ./wait-for-it.sh 
#RUN chmod +x wait-for-it.sh


CMD  npm run start


