const sinon = require('sinon')
const chai = require('chai')
const chaiHttp = require('chai-http')
const { app, db } = require('../server')
const should = chai.should()
chai.use(chaiHttp)
const api = chai.request(app)

describe('Admin Tests', () => {
  before(async () => {
    await db.raw('truncate main')
  })
    it('should pass', (done) => {
      const greaterThanTwenty = sinon.stub().returns('something');
      greaterThanTwenty(0).should.eql('something');
      greaterThanTwenty(0).should.not.eql(false);
      done();
    });

    describe("GET /", () => {
      it('should say hello world', async () => {
        let response = await chai.request(app).get('/')
        response.should.have.status(200)
        response.text.should.eql("hello world")
      })
    })

    describe("GET /people", () => {
      before(async () => {
        await db('main').insert({
          first: "testsdfdf",
          last: "last",
          email: "address@test.com",
          phone: "4168921123",
          location: "ON",
          hobby: "games" 
        })
      })
      
      
      it('returns set of people', async () => {
        let response = await chai.request(app).get('/people')
        response.should.have.status(200)
        response.body[0].first.should.eql("testsdfdf")
      })
    })

    describe("POST /people", () => {
      
      it('creates new entry', async () => {
        let response = await chai.request(app)
          .post('/people')
          .send({
            first: "New", 
            last: "Name",
            email: "test@test.com",
            phone: "416-345-3455",
            location: "Toronto",
            hobby: "gaming"
          })
        response.should.have.status(200)
        response.body[0].first.should.eql("New")
      })
    })
  });
  